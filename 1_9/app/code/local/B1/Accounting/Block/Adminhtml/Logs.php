<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 04.10.2021
 * Time: 12:49
 */
class B1_Accounting_Block_Adminhtml_Logs extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_logs';
        $this->_blockGroup = 'accounting';
        $this->_headerText = 'Logs';
        parent::__construct();
        $this->_removeButton('add');
    }

}
