<?php

class B1_Accounting_Block_Adminhtml_Logs_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('logGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('log_filter');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('accounting/log')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('created_at', array(
            'header' => 'Created At',
            'index' => 'created_at',
            'type' => 'text',
            'width' => '100px',
        ));
        $this->addColumn('is_success', array(
            'header' => 'Status',
            'index' => 'is_success',
            'type' => 'options',
            'width' => '70px',
            'options' => array(
                1 => 'Success',
                0 => 'Error',
            ),
        ));
        $this->addColumn('debug_info', array(
            'header' => 'Debug Info',
            'index' => 'debug_info',
            'type' => 'text',
            'string_limit'  => '50000',
        ));

        $this->addExportType('*/*/exportJson', 'JSON');

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');

        $this->getMassactionBlock()->addItem('export', array(
            'label' => 'Export to JSON',
            'url' => $this->getUrl('*/*/massExport', array('_current' => true)),
        ));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/logsGrid', array('_current' => true));
    }

    public function getJson($paramIds=null)
    {
        $result = array();

        if($paramIds !== null){
            $Ids = implode(',', array_map('intval', $paramIds));
        } else {
            $Ids = $this->getRequest()->getParam('internal_ids');
        }


        $this->_isExport = true;
        $this->_prepareGrid();
        if ($Ids) {
            $Ids = explode(',', $Ids);
            $this->getCollection()->addFieldToFilter('id', array('in' => $Ids))->getSelect()->limit();
            $this->getCollection()->setPageSize(0);
            $this->getCollection()->load();
            $this->_afterLoadCollection();
        } else {
            $this->getCollection()->getSelect()->limit();
            $this->getCollection()->setPageSize(0);
            $this->getCollection()->load();
            $this->_afterLoadCollection();
        }


        foreach ($this->getCollection() as $row) {
            $debug_info = json_decode($row->getData('debug_info'), true);
            $is_success = $row->getData('is_success') == 1;
            $result[] = [
                'created_at' => $row->getData('created_at'),
                'is_success' => $is_success,
                'debug_info' => $debug_info
            ];

        }

        return $export = json_encode($result, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    public function getRowUrl($row)
    {
        return false;
    }

}
