<?php

class B1_Accounting_Block_Adminhtml_ResetReferenceId extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_resetReferenceId';
        $this->_blockGroup = 'accounting';
        $this->_headerText = Mage::helper('accounting')->__('ResetReferenceId');
        parent::__construct();
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Reset order reference id')
            ->setOnClick('resetOrderReferenceId()')
            ->toHtml();

        return $html;
    }

}