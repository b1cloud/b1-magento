<?php

class B1_Accounting_Block_Adminhtml_SyncOrders extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_syncOrders';
        $this->_blockGroup = 'accounting';
        $this->_headerText = Mage::helper('accounting')->__('SyncOrders');
        parent::__construct();
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Sync Orders')
            ->setOnClick('syncOrders()')
            ->toHtml();

        return $html;
    }

}