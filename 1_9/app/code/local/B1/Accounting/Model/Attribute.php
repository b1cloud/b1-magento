<?php

class B1_Accounting_Model_Attribute
{

    public function toOptionArray()
    {
        $data = Mage::getModel('accounting/observer')->getImportAttributeItems();
        if ($data) {
            $options = [];
            foreach ($data['data'] as $option) {
                $options[] = [
                    'value' => $option['id'],
                    'label' => Mage::helper('accounting')->__($option['name'])
                ];

            }

            return $options;
        }else{
            return true;
        }
    }

}