<?php

class B1_Accounting_Model_Boolean
{

    public function toOptionArray()
    {
        return [
            [
                'value' => 0, 'label' => Mage::helper('accounting')->__('No')
            ],
            [
                'value' => 1, 'label' => Mage::helper('accounting')->__('Yes')
            ],
        ];
    }

}