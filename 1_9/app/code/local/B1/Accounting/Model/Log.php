<?php

class B1_Accounting_Model_Log extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        parent::_construct();
        $this->_init('accounting/log');
    }

    public static function logsTableName()
    {
        return 'b1_logs';
    }

    public function saveLog($is_success, $debug_info)
    {
        return $this->getResource()->saveLog($is_success, $debug_info);
    }

    public function clearLogs() {
        return $this->getResource()->clearLogs();
    }

    public function initLogTable()
    {
        return $this->getResource()->initLogTable();
    }

}
