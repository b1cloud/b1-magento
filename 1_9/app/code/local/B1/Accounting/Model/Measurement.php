<?php

class B1_Accounting_Model_Measurement
{

    public function toOptionArray()
    {
        $data = Mage::getModel('accounting/observer')->getImportMeasurementItems();
        $options = [];
        foreach ($data['data'] as $option) {
            $options[] = [
                'value' => $option['id'],
                'label' => Mage::helper('accounting')->__($option['name'])
            ];

        }

        return $options;
    }

}