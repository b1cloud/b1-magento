<?php

require_once(Mage::getBaseDir('lib') . '/B1/B1.php');

class B1_Accounting_Model_Observer
{
    const TTL = 3600;
    const MAX_ITERATIONS = 100;

    public function __construct()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        if (Mage::app()->getFrontController()->getRequest()->getPost('importItems') == true) {
            try {
                $this->importItemsToB1();
            } catch (Exception $ex) {
                Mage::logException($ex);
            }
        }
        if (Mage::app()->getFrontController()->getRequest()->getPost('resetOrderReferenceId') == true) {
            try {
                $this->resetOrderReferenceId();
            } catch (Exception $ex) {
                Mage::logException($ex);
            }
        }
        if (Mage::app()->getFrontController()->getRequest()->getPost('syncOrders') == true) {
            try {
                $this->syncOrders();
            } catch (Exception $ex) {
                Mage::logException($ex);
            }
        }
        if (Mage::app()->getFrontController()->getRequest()->getPost('syncProducts') == true) {
            try {
                $this->syncAllProducts();
            } catch (Exception $ex) {
                Mage::logException($ex);
            }
        }
        if (Mage::app()->getFrontController()->getRequest()->getPost('clearLogs') == true) {
            try {
                $this->clearLogs();
            } catch (Exception $ex) {
                Mage::logException($ex);
            }
        }
    }

    public function syncAllProducts()
    {
        try {
            $this->syncB1ProductsWithShop();
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function syncOrders()
    {
        try {
            $this->syncShopOrdersWithB1();
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    /**
     * @param bool $syncAll
     * @return $this
     * @throws B1Exception
     */

    private function syncB1ProductsWithShop()
    {
        $lastId = null;
        $apiKey = Mage::getStoreConfig('accounting/config/api_key');
        $privateKey = Mage::getStoreConfig('accounting/config/private_key');
        $maxRequestsToApi = Mage::getStoreConfig('accounting/config/max_requests_to_api_per_session');
        $listSize = Mage::getStoreConfig('accounting/config/list_size');
        $enableQuantitySync = Mage::getStoreConfig('accounting/config/enable_quantity_sync');
        $enableNameSync = Mage::getStoreConfig('accounting/config/enable_name_sync');
        $enablePriceSync = Mage::getStoreConfig('accounting/config/enable_price_sync');

        $model = Mage::getModel('accounting/product');
        $b1 = new B1([
            'apiKey' => $apiKey, 'privateKey' => $privateKey
        ]);
        $model->resetAllB1ReferenceId();
        $page = 0;

        $response = $b1->request('e-commerce/config/get');
        $content = $response->getContent();
        $warehouseId = $content['data']['warehouseId'];
        do {
            try {
                $response = $b1->request('e-commerce/items/stock', [
                    'warehouseId' => $warehouseId,
                    'page' => ++$page,
                    'pageSize' => 100,
                    'filters' => [
                        'groupOp' => 'AND',
                        'rules' => [
                        ],
                    ],
                ]);
                $data = $response->getContent();
                foreach ($data['data'] as $product) {
                    $model->updateCode($product['code'], $product['id']);

                    if ($enableQuantitySync) {
                        $model->updateQuantity($product);
                    }
                    if ($enableNameSync) {
                        $model->updateName($product);
                    }
                    if ($enablePriceSync) {
                        $model->updatePrice($product);
                    }
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                $content = $e->getResponse()->getContent();
                header('Content-Type: application/json');
                echo json_encode([
                    'result' => $message . print_r($content, true)
                ]);
                exit;
            }
        } while ($page < $data['pages'] && $page < $maxRequestsToApi);
        Mage::getConfig()->saveConfig('accounting/config/initial_product_sync_done', 1)->cleanCache();
        return $this;
    }

    /**
     * @return $this|bool
     * @throws B1Exception|Exception
     */
    private function syncShopOrdersWithB1()
    {

        try {

//            $initialProductSyncDone = Mage::getStoreConfig('accounting/config/initial_product_sync_done');
//            if (!$initialProductSyncDone) {
//                throw new Exception("Initial product sync is not done yet.");
//            }
            $testSqlMapping = Mage::getStoreConfig("mapping/datafields/orderdate");
            if (!$testSqlMapping || $testSqlMapping == "") {
                throw new Exception("Not set FieldMapping values. You mus again enable B1 plugin. Disable first.");
            }

            $initialOrderSyncDone = Mage::getStoreConfig('accounting/config/initial_order_sync_done');
            $writeOff = $initialOrderSyncDone ? Mage::getStoreConfig('accounting/config/enable_write_off') : false;
            $enableQuantitySync = Mage::getStoreConfig('accounting/config/enable_quantity_sync');

            $apiKey = Mage::getStoreConfig('accounting/config/api_key');
            $privateKey = Mage::getStoreConfig('accounting/config/private_key');
            $b1 = new B1([
                'apiKey' => $apiKey, 'privateKey' => $privateKey
            ]);
            $model = Mage::getModel('accounting/product');
            $modelOrder = Mage::getModel('accounting/order');
            $orders = $modelOrder->findAllToSync();

            if ($enableQuantitySync) {
                $response = $b1->request('e-commerce/config/get');
                $content = $response->getContent();
                $warehouseId = $content['data']['warehouseId'];
            }

            foreach ($orders as $order) {
                $order_data = $modelOrder->getOrderSyncData($order, $writeOff);
                try {
                    $result = $b1->request('e-commerce/orders/create', $order_data);
                    $data = $result->getContent();
                    $orderId = $data['data']['id'];
                    $modelOrder->updateB1ReferenceId($order['entity_id'], $orderId);
                } catch (B1DuplicateException $e) {
                    $content = $e->getResponse()->getContent();
                    $modelOrder->updateB1ReferenceId($order['entity_id'], $content['data']['id']);
                    continue;
                } catch (Exception $e) {
                    $message = $e->getMessage();
                    $content = $e->getResponse()->getContent();
                    header('Content-Type: application/json');
                    echo json_encode([
                        'result' => "orderId: " . $order_data['internalId'] . " " . $message . print_r($content, true)
                    ]);
                    exit;
                }
                $itemData = $modelOrder->getOrderItemSyncData($order['entity_id']);
                try{
                    $ids = [];
                    foreach ($itemData as $i => $item) {
                        $item['orderId'] = $orderId;
                        $result = $b1->request('e-commerce/order-items/create', $item);
                        $ids[] = $item['itemId'];
                    }

                    if ($writeOff) {
                        $data = [
                            'orderId' => $orderId,
                        ];
                        $result = $b1->request('e-commerce/orders/create-write-off', $data);
                    }
                    $data = [
                        'orderId' => $orderId,
                        'series' => isset($order_data['invoiceSeries']) ? $order_data['invoiceSeries']: null,
                        'number' => isset($order_data['invoiceNumber']) ? $order_data['invoiceNumber']: null,
                    ];
                    $result = $b1->request('e-commerce/orders/create-sale', $data);

                    if ($enableQuantitySync) {
                        $page = 0;
                        do {
                            $data = [
                                'warehouseId' => $warehouseId,
                                'page' => ++$page,
                                'pageSize' => 100,
                                'filters' => [
                                    'groupOp' => 'AND',
                                    'rules' => [
                                        [
                                            'field' => 'id',
                                            'op' => 'in',
                                            'data' => $ids
                                        ]
                                    ],
                                ],
                            ];
                            $response = $b1->request('e-commerce/items/stock', $data);
                            $retStocks = $response->getContent()['data'];
                            foreach ($retStocks as $stock) {
                                $model->updateQuantity($stock);
                            }
                        } while ($page < $retStocks['pages'] && $page < self::MAX_ITERATIONS);
                    }
                } catch (Exception $e) {
                    $message = $e->getMessage();
                    $content = $e->getResponse()->getContent();
                    header('Content-Type: application/json');
                    echo json_encode([
                        'result' => "orderId: " . $order_data['internalId'] . " " . $message . print_r($content, true)
                    ]);

                    $data = [
                        'internalId' => $order_data['internalId'],
                        'shopId' => $order_data['shopId'],
                    ];
                    $result = $b1->request('e-commerce/orders/delete', $data);
                    $modelOrder->updateB1ReferenceId($order['entity_id'], null);
                    exit;
                }
            }
            if (!$initialOrderSyncDone) {
                Mage::getConfig()->saveConfig('accounting/config/initial_order_sync_done', 1)->cleanCache();
            }
            return $this;

        } catch (Exception $e) {
            $message = $e->getMessage();
            header('Content-Type: application/json');
            echo json_encode([
                'result' => $message
            ]);
            exit;
        }
    }

    /**
     * @return mixed
     * @throws B1Exception
     */

    public function getImportAttributeItems()
    {
        try{
            $apiKey = Mage::getStoreConfig('accounting/config/api_key');
            $privateKey = Mage::getStoreConfig('accounting/config/private_key');

            if (isset($apiKey) && isset($privateKey) && !empty($apiKey) && !empty($privateKey)) {
                $b1 = new B1([
                    'apiKey' => $apiKey,
                    'privateKey' => $privateKey
                ]);
                $data = [
                    'page' => 1,
                    'rows' => 100,
                    'filters' => [
                        'groupOp' => 'AND',
                    ],
                ];
            } else {
                return false;
            }
            $resultAttributeId = $b1->request('reference-book/item-attributes/list', $data);
            $AttributeId = $resultAttributeId->getContent();
            return $AttributeId;
        } catch (Exception $e){
            print_r($e->getMessage());
        }

    }

    /**
     * @return mixed
     * @throws B1Exception
     */

    public function getImportMeasurementItems()
    {
        try{
            $apiKey = Mage::getStoreConfig('accounting/config/api_key');
            $privateKey = Mage::getStoreConfig('accounting/config/private_key');
            if (isset($apiKey) && isset($privateKey) && !empty($apiKey) && !empty($privateKey)) {

                $b1 = new B1([
                    'apiKey' => $apiKey, 'privateKey' => $privateKey
                ]);
                $data = [
                    'page' => 1,
                    'rows' => 100,
                    'filters' => [
                        'groupOp' => 'AND',
                    ],
                ];
            } else {
                return false;
            }
            $resultMeasurementId = $b1->request('reference-book/measurement-units/list', $data);
            $MeasurementId = $resultMeasurementId->getContent();
            return $MeasurementId;

        } catch (Exception $e){
            print_r($e->getMessage());
        }
    }

    /**
     * @throws B1Exception
     */

    public function importItemsToB1()
    {
        $attributeId = $_POST['attributeId'];
        $measurementId = $_POST['measurementId'];
        $apiKey = Mage::getStoreConfig('accounting/config/api_key');
        $privateKey = Mage::getStoreConfig('accounting/config/private_key');
        $b1 = new B1([
            'apiKey' => $apiKey, 'privateKey' => $privateKey
        ]);
        $lid = null;
        $i = 0;
        $k = 0;
        $success = 0;
        $fail = 0;
        $errors = [];
        $model = Mage::getModel('accounting/product');
        $itemCount = $model->fetchAllItemCount();
        for ($i = 0; $i <= ($itemCount / self::MAX_ITERATIONS); $i++) {
            $items = $model->fetchAllItems();
            foreach ($items as $item) {
                $item_data = $this->generate_item_data($item, $attributeId, $measurementId);
                try {
                    $k++;
                    $response = $b1->request('reference-book/items/create', $item_data['data']);
                    $success++;
                    $content = $response->getContent();
                    $model->updateCode($item['sku'], $content['data']['id']);
                } catch (B1Exception $e) {
                    $fail++;
                    $content = $e->getResponse()->getContent();
                    $errors['errorMessages'][$k] = [
                        $content['errors']['code'][0],
                    ];
                }
            }
        }
        $errors['success'] = [
            'success' => $success,
        ];
        $errors['failed'] = [
            'failed' => $fail,
        ];
        header('Content-Type: application/json');
        echo json_encode($errors);
        exit;
    }


    public function resetOrderReferenceId()
    {
        $modelOrder = Mage::getModel('accounting/order');
        try {
            $modelOrder->reset_order_reference_id();
        } catch (Exception $e) {
            echo json_encode([
                'result' => $e
            ]);
        }
        header('Content-Type: application/json');
        echo json_encode([
            'result' => 'Success'
        ]);
        exit;
    }

    private function generate_item_data($item, $attributeId, $measurementId)
    {
        $product = Mage::getModel('catalog/product')->load($item['entity_id']);
        $item_data = [];
        $item_data['code'] = $product->getData('sku');
        $item_data['name'] = $product->getData('name');
        $item_data['attributeId'] = $attributeId;
        $item_data['measurementUnitId'] = $measurementId;
        $item_data['priceWithoutVat'] = round($product->getData('price'), 2);
        $item_data['description'] = $product->getData('description');

        return [
            'data' => $item_data
        ];
    }

    public function clearLogs(){
        $model = Mage::getModel('accounting/log');
        $model->clearLogs();
    }

}
