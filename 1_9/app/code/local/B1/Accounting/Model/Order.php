<?php

class B1_Accounting_Model_Order extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {

        $this->_init('accounting/order');
    }

    public function findAllToSync()
    {
        return $this->getResource()->findAllToSync();
    }

    public function findOneToSync($orderId)
    {
        return $this->getResource()->findOneToSync($orderId);
    }
    
    public function getValueUsingSql($optionStr,$orderId,$itemId = null)
    {
        $sql = Mage::getStoreConfig($optionStr);
        if (!($sql > "")) return "";
        if (substr_count($sql,"%d") > 1){
            $query = str_replace("%d0",(string)$orderId,$sql);  
            $query = str_replace("%d1",(string)$itemId,$query);
        } else {
            $query = str_replace("%d",(string)$orderId,$sql);
        }
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = $readConnection->fetchOne($query);
        return $result;
    }      

    public function getArrayUsingSql($optionStr,$orderId)
    {
        $sql = Mage::getStoreConfig($optionStr);
        if (!($sql > "")) return null;
        $query = str_replace("%d",(string)$orderId,$sql);
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        return $readConnection->fetchAll($query);
    }      


    public function getOrderSyncData($order)
    {
        $shopId = Mage::getStoreConfig('accounting/config/shop_id');
        if (!(Mage::getStoreConfig("mapping/info") > "")) $this->initCoreOrderSqlOptions();
        $syncInvoices = Mage::getStoreConfig('accounting/config/enable_sync_invoices');
        $data = array(
            'shopId' => $shopId,
            'internalId' => $order['entity_id'],
            'date' => date('Y-m-d', strtotime($this->getValueUsingSql("mapping/datafields/orderdate",$order['entity_id']))),
            'number' => strval($this->getValueUsingSql("mapping/datafields/orderno",$order['entity_id'])),
            'currencyCode' => $this->getValueUsingSql("mapping/datafields/currency",$order['entity_id']),
            'shipping' => round( (float)$this->getValueUsingSql("mapping/datafields/shippingamount",$order['entity_id']), 2),
            'gift' => round( $this->getValueUsingSql("mapping/datafields/gift",$order['entity_id']),2),
            'discount' => round( (float)$this->getValueUsingSql("mapping/datafields/discount",$order['entity_id']),2),
            'total' => round( (float)$this->getValueUsingSql("mapping/datafields/total",$order['entity_id']), 2),
            'email' => $this->getValueUsingSql("mapping/datafields/orderemail",$order['entity_id']),
        );
        if ($syncInvoices > 0) {
            $data['invoiceSeries'] = $this->getValueUsingSql("mapping/datafields/invoiceseries", $order['entity_id']);
            $data['invoiceNumber'] = $this->getValueUsingSql("mapping/datafields/invoicenumber", $order['entity_id']);
        }
        $data['billing'] = [];
        $data['delivery'] = [];

        $firstname = $this->getValueUsingSql("mapping/datafields/billingfirstname",$order['entity_id']);
        $lastname = $this->getValueUsingSql("mapping/datafields/billinglastname",$order['entity_id']);
        $data['billing'] = [
            'isJuridical' => empty($this->getValueUsingSql("mapping/datafields/billingiscompany",$order['entity_id'])) ? 0 : 1,
            'name' => $firstname . " " . $lastname,
            'vatCode' => $this->getValueUsingSql("mapping/datafields/billingvatcode",$order['entity_id']),
            'code' => $this->getValueUsingSql("mapping/datafields/billingcode",$order['entity_id']),
            'address' => $this->getValueUsingSql("mapping/datafields/billingaddress",$order['entity_id']),
            'cityName' => $this->getValueUsingSql("mapping/datafields/billingcity",$order['entity_id']),
            'postcode' => $this->getValueUsingSql("mapping/datafields/billingpostcode",$order['entity_id']),
            'countryCode' => $this->getValueUsingSql("mapping/datafields/billingcountry",$order['entity_id']),
        ];
        $firstname = $this->getValueUsingSql("mapping/datafields/deliveryfirstname",$order['entity_id']);
        $lastname = $this->getValueUsingSql("mapping/datafields/deliverylastname",$order['entity_id']);
        $data['delivery'] = [
            'isJuridical' => empty($this->getValueUsingSql("mapping/datafields/deliveryiscompany",$order['entity_id'])) ? 0 : 1,
            'name' => $firstname . " " . $lastname,
            'vatCode' => $this->getValueUsingSql("mapping/datafields/deliveryvatcode",$order['entity_id']),
            'code' => $this->getValueUsingSql("mapping/datafields/deliverycode",$order['entity_id']),
            'address' => $this->getValueUsingSql("mapping/datafields/deliveryaddress",$order['entity_id']),
            'cityName' => $this->getValueUsingSql("mapping/datafields/deliverycity",$order['entity_id']),
            'postcode' => $this->getValueUsingSql("mapping/datafields/deliverypostcode",$order['entity_id']),
            'countryCode' => $this->getValueUsingSql("mapping/datafields/deliverycountry",$order['entity_id']),
        ];
        return $data;
    }

    public function getOrderItemSyncData($orderId){

        $orderItems = $this->getArrayUsingSql("mapping/datafields/items",$orderId);
        $data = [];
        foreach ($orderItems as $i => $item) {
            $data['items'][$i] = array(
                'itemId' => $item['b1_reference_id'],
                'name' => $this->getValueUsingSql("mapping/datafields/itemsname",$orderId,$item['item_id']),
                'quantity' => round((float)$this->getValueUsingSql("mapping/datafields/itemsquantity",$orderId,$item['item_id']),3),
                'price' => round( (float)$this->getValueUsingSql("mapping/datafields/itemsprice",$orderId,$item['item_id']), 4),
                'sum' => round( (float)$this->getValueUsingSql("mapping/datafields/itemssum",$orderId,$item['item_id']), 2),
                'vatRate' => (int) $this->getValueUsingSql("mapping/datafields/itemsvatrate",$orderId,$item['item_id']),
            );
        }
        return $data['items'];
    }
    
    public function initCoreOrderSqlOptions()
    {
        $base = Mage::getSingleton('core/resource');
        $optionsSql = array(
            'invoiceseries' =>     "",
            'invoicenumber' =>     "",

            'orderdate' =>      "SELECT created_at \nFROM {$base->getTableName('sales_flat_order')} \nWHERE entity_id= %d",
            'orderno' =>        "SELECT entity_id \nFROM {$base->getTableName('sales_flat_order')} \nWHERE entity_id = %d",
            'currency' =>       "SELECT order_currency_code \nFROM  {$base->getTableName('sales_flat_order')} \nWHERE entity_id = %d",
            'discount' =>       "SELECT discount_amount \nFROM {$base->getTableName('sales_flat_order')} \nWHERE entity_id = %d",
            'total' =>          "SELECT grand_total \nFROM  {$base->getTableName('sales_flat_order')} \nWHERE entity_id = %d",
            'orderemail' =>     "SELECT customer_email \nFROM {$base->getTableName('sales_flat_order')} \nWHERE entity_id = %d",
            'shippingamount' => "SELECT shipping_amount + shipping_tax_amount \nFROM {$base->getTableName('sales_flat_order')} \nWHERE entity_id = %d",
            'gift' =>           "",
            
            'billingfirstname'=> "SELECT firstname \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            'billinglastname'=>  "SELECT lastname \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            'billingiscompany'=> "SELECT company \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            'billingvatcode'=>   "SELECT vat_id \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            'billingcode'=>   "",
            'billingaddress'=>   "SELECT street \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            'billingcity' =>     "SELECT city \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            'billingpostcode' => "SELECT postcode \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            'billingcountry' =>  "SELECT country_id \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.billing_address_id \nWHERE p0.entity_id = %d",
            
            'deliveryfirstname'=> "SELECT firstname \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",
            'deliverylastname'=>  "SELECT lastname \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",
            'deliveryiscompany'=> "SELECT company \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",            
            'deliveryvatcode'=>   "SELECT vat_id \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",
            'deliverycode'=>   "",
            'deliveryaddress'=>   "SELECT street \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",
            'deliverycity'=>      "SELECT city \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",
            'deliverypostcode'=>  "SELECT postcode \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",
            'deliverycountry'=>   "SELECT country_id \nFROM {$base->getTableName('sales_flat_order')} p0 \nLEFT JOIN {$base->getTableName('sales_flat_order_address')} pm1 ON pm1.entity_id = p0.shipping_address_id \nWHERE p0.entity_id = %d",
            
            "items" =>            "SELECT * FROM {$base->getTableName('sales_flat_order_item')} p0 left join  {$base->getTableName('catalog_product_entity')}  p1 on p1.entity_id = p0.product_id \nWHERE p0.order_id  = %d",
            'itemsname'=>         "SELECT name FROM {$base->getTableName('sales_flat_order_item')} p0 \nWHERE  p0.item_id = %d1 AND p0.order_id = %d0",        
            'itemsquantity'=>     "SELECT qty_ordered FROM {$base->getTableName('sales_flat_order_item')} p0 \nWHERE  p0.item_id = %d1 AND p0.order_id = %d0",
            'itemsprice' =>       "SELECT price_incl_tax FROM {$base->getTableName('sales_flat_order_item')} p0 \nWHERE  p0.item_id = %d1 AND p0.order_id = %d0",
            'itemssum' =>         "SELECT row_total_incl_tax FROM {$base->getTableName('sales_flat_order_item')} p0 \nWHERE  p0.item_id = %d1 AND p0.order_id = %d0",
            'itemsvatrate' =>     "SELECT tax_percent FROM {$base->getTableName('sales_flat_order_item')} p0 \nWHERE  p0.item_id = %d1 AND p0.order_id = %d0",
        );   
          
        Mage::getConfig()->saveConfig("mapping/info", "SQL's to get datafields or data for request to B1 (https://www.b1.lt/help/eshop-parametrai)");        
        foreach ($optionsSql as $key=>$sql){
            Mage::getConfig()->saveConfig("mapping/datafields/$key", $sql)->cleanCache();
        }
    }        

    public function updateB1ReferenceId($orderId, $referenceId)
    {
        $this->getResource()->updateB1ReferenceId($orderId, $referenceId);
        return $this;
    }

    private function convertFloatToInt($value)
    {
        return intval(round($value * 100));
    }

    public function getBillingAddress($billingId)
    {
        return $this->getResource()->getBillingAddress($billingId);
    }

    public function getShippingAddress($shippingId)
    {
        return $this->getResource()->getShippingAddress($shippingId);
    }

    public function getOrderItems($orderId)
    {
        return $this->getResource()->getOrderItems($orderId);
    }

    public function reset_order_reference_id()
    {
        return $this->getResource()->reset_order_reference_id();
    }


}
