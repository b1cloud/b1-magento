<?php

class B1_Accounting_Model_Product extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('accounting/product');
    }

    public function resetAllB1ReferenceId()
    {
        $this->getResource()->resetAllB1ReferenceId();
        return $this;
    }

    public function updateCode($code, $referenceId)
    {
        $this->getResource()->updateCode($code, $referenceId);
        return $this;
    }

    public function updateQuantity($product)
    {
        $this->getResource()->updateQuantity($product);
        return $this;
    }

    public function updateName($product)
    {
        $this->getResource()->updateName($product);
        return $this;
    }

    public function updatePrice($product)
    {
        $this->getResource()->updatePrice($product);
        return $this;
    }

    public function fetchAllItems()
    {
        $entityId = $this->getResource()->fetchAllItems();
        return $entityId;
    }

    public function fetchAllItemCount()
    {
        $count = $this->getResource()->fetchAllItemCount();
        return $count[0]['count'];
    }
}