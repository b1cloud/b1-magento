<?php

class B1_Accounting_Model_Resource_Log extends Mage_Core_Model_Resource_Db_Abstract
{

    public $table;


    protected function _construct()
    {
        $this->table = $this->getTable(B1_Accounting_Model_Log::logsTableName());
        $this->_init('accounting/log', 'id');

    }

    public function saveLog($is_success, $debug_info)
    {

        try {

            $this->_getWriteAdapter()->insert($this->table, [
                'is_success' => $is_success,
                'debug_info' => json_encode($debug_info, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT),
            ]);

        } catch (\Exception $e) {
        }
    }

    public function clearLogs()
    {
        try {
            $time = date('Y-m-d H:i:s', strtotime('-7 day'));

            $this->_getWriteAdapter()->query("DELETE FROM ".$this->table."  WHERE `created_at` < '{$time}'");

        } catch (\Exception $e) {
        }
    }

    public function initLogTable(){
        try {

            $this->_getWriteAdapter()->query("CREATE TABLE IF NOT EXISTS {$this->table}(
            id bigint(11) NOT NULL AUTO_INCREMENT,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            is_success smallint,
            debug_info text,
            PRIMARY KEY  (id) 
            )");

        } catch (\Exception $e) {
        }

    }


}
