<?php

class B1_Accounting_Model_Resource_Log_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('accounting/log');
    }
}
