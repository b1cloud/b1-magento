<?php

class B1_Accounting_Model_Resource_Order extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('accounting_resource/order', 'entity_id');

    }

    public function findAllToSync()
    {
        $statusName = Mage::getStoreConfig('accounting/config/order_status_name_check');
        $orderSyncFrom = Mage::getStoreConfig('accounting/config/order_sync_from');
        $sql = "SELECT * 
        FROM {$this->getTable('sales/order')} 
        WHERE b1_reference_id IS NULL AND created_at>=:f AND (status=:os OR entity_id IN (SELECT parent_id FROM {$this->getTable('sales/order_status_history')} WHERE status=:os)) LIMIT 100";
        $orders = $this->_getWriteAdapter()->fetchAll($sql, array(
            'os' => $statusName,
            'f' => $orderSyncFrom,
        ));
        return $orders;
    }
    // Funkcija testavimui
    public function findOneToSync($orderId)
    {
        $orderSyncFrom = Mage::getStoreConfig('accounting/config/order_sync_from');
        $sql = "SELECT * 
        FROM {$this->getTable('sales/order')} 
        WHERE b1_reference_id IS NULL AND created_at>=:f 
        AND entity_id =:o";
        $orders = $this->_getWriteAdapter()->fetchAll($sql, array(
            'o' => $orderId,
            'f' => $orderSyncFrom,
        ));
        return $orders;
    }

    public function updateB1ReferenceId($orderId, $referenceId)
    {
        if ($referenceId == null) {
            $sql = "UPDATE {$this->getTable('sales/order')} SET b1_reference_id = NULL WHERE entity_id = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'o' => $orderId,
            ));
        } else {
            $sql = "
            UPDATE {$this->getTable('sales/order')} 
            SET b1_reference_id = :r 
            WHERE entity_id = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'r' => $referenceId,
                'o' => $orderId,
            ));
        }
        return $this;
    }


    public function getBillingAddress($billingId)
    {
        $sql = "
        SELECT * 
        FROM {$this->getTable('sales/order_address')} 
        WHERE entity_id =:b";
        $billingAddress = $this->_getWriteAdapter()->fetchAll($sql, array(
            'b' => $billingId,
        ));
        return $billingAddress;
    }

    public function getShippingAddress($shippingId)
    {
        $sql = "
        SELECT * 
        FROM {$this->getTable('sales/order_address')} 
        WHERE entity_id =:s ";
        $shippingAddress = $this->_getWriteAdapter()->fetchAll($sql, array(
            's' => $shippingId,
        ));
        return $shippingAddress;
    }

    public function getOrderItems($orderId)
    {
        $sql = "
        SELECT * 
        FROM {$this->getTable('sales/order_item')} sfoi
        LEFT JOIN {$this->getTable('catalog/product')} as cpe on cpe.entity_id = sfoi.product_id  
        WHERE order_id =:o ";
        $orderItems = $this->_getWriteAdapter()->fetchAll($sql, array(
            'o' => $orderId,
        ));
        return $orderItems;
    }


    public function reset_order_reference_id()
    {
        $sql = "
            UPDATE {$this->getTable('sales/order')} 
            SET b1_reference_id = null";
        $this->_getWriteAdapter()->query($sql);
    }
}
