<?php

class B1_Accounting_Model_Resource_Product extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('accounting_resource/product', 'entity_id');

    }

    public function resetAllB1ReferenceId()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} SET `b1_reference_id`=:r", array(
            'r' => null,
        ));
        return $this;
    }

    public function updateCode($code, $referenceId)
    {
        $referenceId = (int)$referenceId;
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} SET `b1_reference_id`=:r WHERE `sku`=:id", array(
            'r' => $referenceId,
            'id' => $code,
        ));
        return $this;
    }

    public function updateQuantity($product)
    {
        $id = (int)$product['id'];
        $quantity = (int)$product['quantity'];
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_item')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_status')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        return $this;
    }
    public function updateName($product)
    {
        $id = (int)$product['id'];
        $name = $product['name'];
        $this->_getWriteAdapter()->query("UPDATE  catalog_product_entity_varchar cp LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON cp.`entity_id` = p.`entity_id` SET cp.`value` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $name,
            'id' => $id,
        ));
        return $this;
    }
    public function updatePrice($product)
    {
        $id = (int)$product['id'];
        $price = $product['priceWithoutVat'];
        $this->_getWriteAdapter()->query("
UPDATE  catalog_product_entity_decimal cp 
LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON cp.`entity_id` = p.`entity_id` 
SET cp.`value` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $price,
            'id' => $id,
        ));
        return $this;
    }

    public function fetchAllItems()
    {
        $sql = "
         SELECT entity_id, sku
         FROM {$this->getTable('catalog/product')} 
         WHERE `b1_reference_id` is null AND sku != ' ' AND sku is not null LIMIT 100";
        $items = $this->_getWriteAdapter()->fetchAll($sql);
        return $items;
    }

    public function fetchAllItemCount()
    {
        $sql = "
         SELECT count(*) as count
         FROM {$this->getTable('catalog/product')} 
         WHERE `b1_reference_id` is null AND sku != ' ' AND sku is not null";
        $items = $this->_getWriteAdapter()->fetchAll($sql);
        return $items;
    }

    public function fetchB1referenceId($itemId){
        $sql = "
         SELECT b1_reference_id 
         FROM {$this->getTable('catalog/product')} 
         WHERE `entity_id` = :q limit 1";
        $referenceId = $this->_getWriteAdapter()->fetchOne($sql, array(
            ':q'=>$itemId,
        ));
        return $referenceId;
    }

}
