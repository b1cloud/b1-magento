Įskiepis skirtas sinchronizuoti produktus tarp Magento ir B1.lt aplikacijos.

# Magento 2.1.x #

### Reikalavimai ###

@TODO

### Diegimas ###

@TODO

# Magento 1.9 #

### Reikalavimai ###

* PHP 5.5
* Magento 1.9.2.4

### Diegimas ###

* SVARBU! Perskaitykite [dokumentacija](https://www.b1.lt/help/eshop-parametrai) ir atlikite nurodytus ten veiksmus.
* [NEBŪTINA] Pasidarykite failų atsarginę kopiją.
* Padaryti atsarginę DB kopiją.
* Perkelkite failus iš "1_9" direktorijos į magento direktoriją.
* Administracijos skiltyje atidarykite "System/Configuration/B1 plugin/Configuration settings" ir suveskite reikiamą informaciją. Pastaba: Jeigu nerandate "B1 plugin" punkto - pabandykite išvalyti magento cache'a (System/Cache management/Clear magento cache) ir prisijungti iš naujo.
* Nustatykite cron (System/Configuration/B1 plugin/Configuration settings/CRON rules) vykdymo laikus. Pastaba: Magento cron vykdytojas (System/Configuration/Advanced/System/Cron (Scheduled Tasks) - all the times are in minutes) gali ne iš karto sugeneruoti darbus.
* Cron darbai (Sync Orders, Fetch all products) turi susinchronizuoti užsakymus ir gauti prekės iš B1.

### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt